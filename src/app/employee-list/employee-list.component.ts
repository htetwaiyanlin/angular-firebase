import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  private employeeCollection: AngularFirestoreCollection<any>;
  employee: Observable<any[]>;

  constructor(private afs: AngularFirestore) {
    this.employeeCollection = this.afs.collection<any>('employee');
    this.employee = this.employeeCollection.valueChanges();
   }

  ngOnInit() {
    console.log('Helo');
  }

}
