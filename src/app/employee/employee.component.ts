import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  obj = {id: '', name: '', dob: '', nrc: '', gender: ''};

  private employeeCollection: AngularFirestoreCollection<any>;
  employee: Observable<any[]>;


  constructor(private afs: AngularFirestore) {
    this.employeeCollection = afs.collection<any>('employee');
   }

  ngOnInit() {
  }


  addEmployee(employee: any) {
    this.employeeCollection.add(employee);
  }

  clear() {
    this.obj = {id: '', name: '', dob: '', nrc: '', gender: ''};
  }

}
